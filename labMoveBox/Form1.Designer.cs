﻿namespace labMoveBox
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.hpbar = new System.Windows.Forms.ProgressBar();
            this.hp_text = new System.Windows.Forms.Label();
            this.timer_hp = new System.Windows.Forms.Timer(this.components);
            this.yd = new System.Windows.Forms.Label();
            this.timer_heal = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.heal = new System.Windows.Forms.PictureBox();
            this.pb1 = new System.Windows.Forms.PictureBox();
            this.pic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.heal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 50;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // hpbar
            // 
            this.hpbar.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.hpbar.Location = new System.Drawing.Point(2, 30);
            this.hpbar.Name = "hpbar";
            this.hpbar.Size = new System.Drawing.Size(194, 36);
            this.hpbar.TabIndex = 3;
            // 
            // hp_text
            // 
            this.hp_text.AutoSize = true;
            this.hp_text.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.hp_text.ForeColor = System.Drawing.Color.ForestGreen;
            this.hp_text.Location = new System.Drawing.Point(83, 2);
            this.hp_text.Name = "hp_text";
            this.hp_text.Size = new System.Drawing.Size(41, 25);
            this.hp_text.TabIndex = 4;
            this.hp_text.Text = "HP";
            // 
            // timer_hp
            // 
            this.timer_hp.Enabled = true;
            this.timer_hp.Interval = 190;
            this.timer_hp.Tick += new System.EventHandler(this.timer_hp_Tick);
            // 
            // yd
            // 
            this.yd.AutoSize = true;
            this.yd.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.yd.ForeColor = System.Drawing.Color.Red;
            this.yd.Location = new System.Drawing.Point(356, 184);
            this.yd.Name = "yd";
            this.yd.Size = new System.Drawing.Size(185, 42);
            this.yd.TabIndex = 5;
            this.yd.Text = "Проиграл";
            this.yd.Visible = false;
            // 
            // timer_heal
            // 
            this.timer_heal.Enabled = true;
            this.timer_heal.Interval = 4000;
            this.timer_heal.Tick += new System.EventHandler(this.timer_heal_Tick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.LimeGreen;
            this.label1.Location = new System.Drawing.Point(938, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 29);
            this.label1.TabIndex = 7;
            this.label1.Text = "0:00";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(464, 273);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 28);
            this.button1.TabIndex = 8;
            this.button1.Text = "Новая игра";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // heal
            // 
            this.heal.Image = global::labMoveBox.Properties.Resources.healthpack01;
            this.heal.Location = new System.Drawing.Point(612, 273);
            this.heal.Name = "heal";
            this.heal.Size = new System.Drawing.Size(41, 53);
            this.heal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.heal.TabIndex = 6;
            this.heal.TabStop = false;
            this.heal.Visible = false;
            // 
            // pb1
            // 
            this.pb1.Image = global::labMoveBox.Properties.Resources.Dogr;
            this.pb1.Location = new System.Drawing.Point(559, 95);
            this.pb1.Name = "pb1";
            this.pb1.Size = new System.Drawing.Size(118, 100);
            this.pb1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb1.TabIndex = 1;
            this.pb1.TabStop = false;
            // 
            // pic
            // 
            this.pic.Image = global::labMoveBox.Properties.Resources.k1N90px_5LU;
            this.pic.Location = new System.Drawing.Point(410, 136);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(72, 45);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic.TabIndex = 0;
            this.pic.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 516);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.heal);
            this.Controls.Add(this.yd);
            this.Controls.Add(this.hp_text);
            this.Controls.Add(this.hpbar);
            this.Controls.Add(this.pb1);
            this.Controls.Add(this.pic);
            this.MaximumSize = new System.Drawing.Size(1024, 555);
            this.MinimumSize = new System.Drawing.Size(1024, 555);
            this.Name = "Form1";
            this.Text = "s";
            ((System.ComponentModel.ISupportInitialize)(this.heal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pb1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.PictureBox pb1;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ProgressBar hpbar;
        private System.Windows.Forms.Label hp_text;
        private System.Windows.Forms.Timer timer_hp;
        private System.Windows.Forms.Label yd;
        private System.Windows.Forms.Timer timer_heal;
        private System.Windows.Forms.PictureBox heal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}

